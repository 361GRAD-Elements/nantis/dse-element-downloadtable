<?php

/**
 * 361GRAD Element Download Table
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementDownloadTable\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\File;
use Contao\FilesModel;
use Contao\StringUtil;
use Exception;

/**
 * Class ContentDseDownloadTableEntry
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseDownloadTableEntry extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_downloadtable_entry';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->title    = $this->headline;
            $objTemplate->wildcard = $this->dse_downloadtitle;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return boolean
     */
    protected function compile()
    {
        $uuid = $this->dse_downloadentry;

        if (!$uuid) {
            return null;
        }

        $download = FilesModel::findByUuid($uuid);

        if (!$download) {
            return null;
        }

        try {
            $file = new File($download->path, true);
            if (!$file->exists()) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        $filesize = filesize($download->path);

        if($filesize < 1000000) {
            $fileData = [
                'id'   => $download->id,
                'uuid' => isset($download->uuid) ? $download->uuid : null,
                'path' => $download->path,
                'name' => $this->dse_downloadtitle,
                'size' => $this->humanFilesize($filesize, 0) / 1000,
            ];
        } else {
            $fileData = [
                'id'   => $download->id,
                'uuid' => isset($download->uuid) ? $download->uuid : null,
                'path' => $download->path,
                'name' => $this->dse_downloadtitle,
                'size' => $this->humanFilesize($filesize, 1),
            ];
        }

        $this->Template->fileData = $fileData;

        // Build subheadline like Contao headline
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';

        return true;
    }

    /**
     * Translate bytes-format filesize into human readable
     *
     * @param int $bytes    Bytes of file.
     * @param int $decimals Decimals after comma.
     *
     * @return string
     */
    private function humanFilesize($bytes, $decimals = 2)
    {
        $sz     = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", ($bytes / pow(1024, $factor))) . @$sz[intval($factor)];
    }
}
