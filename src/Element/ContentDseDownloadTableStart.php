<?php

/**
 * 361GRAD Element Download Table
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementDownloadTable\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;

/**
 * Class ContentDseDownloadTableStart
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseDownloadTableStart extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_downloadtable_start';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $this->strTemplate = 'be_wildcard';
            $objTemplate       = new BackendTemplate($this->strTemplate);

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
    }
}
