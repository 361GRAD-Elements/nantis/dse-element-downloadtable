<?php

namespace Dse\ElementsBundle\ElementDownloadTable\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementDownloadTable;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementDownloadTable\DseElementDownloadTable::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
